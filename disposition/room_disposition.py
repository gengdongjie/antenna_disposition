import math
from logger import logger
import config
from disposition.plot import plot
from shapely.geometry import Point, LineString, Polygon
from shapely.ops import split, cascaded_union
from shapely import speedups, affinity
from shapely.geometry import MultiPolygon

from structure.antenna import Antenna

speedups.enable()


# return [(x1,y1, radius1),(x2,y2,radius2),...]
def disposition(polygon, max_radius=config.MAX_RADIUS, coverage_threshold=0.5,
                max_antenna_num=1, coverage_gain_threshold=0.3, plot_procedure=False, plot_best=False):
    room = polygon
    next_antenna_num = 1
    last_coverage = 1
    coverage = 2
    split_polygons = None
    cur_antennas = None
    last_antennas = None
    while (coverage / polygon.area < coverage_threshold and
           next_antenna_num <= max_antenna_num):
        if (coverage / last_coverage - 1) < coverage_gain_threshold:
            cur_antennas = last_antennas
            next_antenna_num -= 1
            break
        last_antennas = cur_antennas
        cur_antennas = []
        split_polygons = split_room(room, fraction=next_antenna_num)
        for split_polygon in split_polygons:
            split_point, split_radius = single_antenna_disposition_scale(split_polygon, max_radius=max_radius,
                                                                         container=room)[:2]
            cur_antennas.append(split_point + (split_radius,))
        next_antenna_num += 1
        last_coverage = coverage
        coverage = __get_antennas_coverage(room, cur_antennas)
        if plot_procedure:
            plot(polygons=split_polygons, circles=cur_antennas,
                 title="antenna_num=%s, coverage=%.4f, coverage_gain=%.4f" % (
                     next_antenna_num - 1, (coverage / room.area), (coverage / last_coverage - 1)))

    if plot_best:
        plot(polygons=split_polygons, circles=cur_antennas,
             title="Final result: antenna_num=%s, coverage=%.4f" % (next_antenna_num - 1, (coverage / room.area)))
    return cur_antennas


# return center(x,y), radius, coverage
def single_antenna_disposition_scale(polygon, max_radius=config.MAX_RADIUS,
                                     container=None,
                                     interference_level=config.INTERFERENCE_LEVEL,
                                     plot_best=False, plot_procedure=False):
    room = polygon
    container = room if container is None else container
    if room.area < 4:
        min_x, min_y, max_x, max_y = room.bounds
        min_x = int(math.ceil(min_x))
        min_y = int(math.ceil(min_y))
        max_x = int(math.floor(max_x))
        max_y = int(math.floor(max_y))
    else:
        halved_room = affinity.scale(room, xfact=0.5, yfact=0.5, origin=(0.0, 0.0))
        halved_container = affinity.scale(container, xfact=0.5, yfact=0.5, origin=(0.0, 0.0))
        # logger.debug(max_radius / 2)
        halved_room_point = single_antenna_disposition_scale(halved_room, max_radius=max_radius / 2.0,
                                                             container=halved_container,
                                                             interference_level=interference_level,
                                                             plot_procedure=plot_procedure)[0]
        if halved_room_point is not None:
            # logger.debug("halvedpoint " +str(halved_room_point))
            room_point = (int(halved_room_point[0] * 2), int(halved_room_point[1] * 2))
            min_x = room_point[0] - 1
            max_x = room_point[0] + 1
            min_y = room_point[1] - 1
            max_y = room_point[1] + 1
        else:
            min_x, min_y, max_x, max_y = room.bounds
            min_x = int(math.ceil(min_x))
            min_y = int(math.ceil(min_y))
            max_x = int(math.floor(max_x))
            max_y = int(math.floor(max_y))
    max_coverage = float('-inf')
    max_coverage_point = None
    max_coverage_min_radius = float('inf')
    nearest_centroid_distance = float('inf')
    # logger.debug(" %s"%(list(room.boundary.coords)))
    # logger.debug(" %s, %s,%s,%s" % (min_x,max_x,min_y,max_y))
    for x in range(int(min_x), int(max_x + 1)):
        for y in range(int(min_y), int(max_y + 1)):
            point = Point(x, y)
            if not point.intersects(room):
                continue
            # logger.debug(str(point)+" True")
            if interference_level > 1:
                inside_distance = max(room.boundary.distance(point), container.boundary.distance(point))
                radius = max_radius if inside_distance > max_radius else inside_distance
            else:
                hausdorff_distance = room.hausdorff_distance(point)
                radius = max_radius if hausdorff_distance > max_radius else hausdorff_distance
                # logger.debug("%s,%s,%s" % (hausdorff_distance, max_radius, radius))
            coverage = round(point.buffer(radius, resolution=16).intersection(room).area, 3)
            centroid_distance = point.distance(room.centroid)
            # 1. choose the point who covers more area
            if coverage > max_coverage:
                max_coverage_point = point
                max_coverage = coverage
                max_coverage_min_radius = radius
                nearest_centroid_distance = centroid_distance
            elif coverage == max_coverage:
                # 2. choose the point who has a smaller radius
                if radius < max_coverage_min_radius:
                    max_coverage_point = point
                    max_coverage_min_radius = radius
                    nearest_centroid_distance = centroid_distance
                elif radius == max_coverage_min_radius:
                    # 3. choose the point who is nearer to the centroid
                    if centroid_distance < nearest_centroid_distance:
                        max_coverage_point = point
                        nearest_centroid_distance = centroid_distance
    if plot_procedure or plot_best:
        plot(polygons=[room, max_coverage_point.buffer(max_coverage_min_radius)], points=[max_coverage_point])
    # logger.debug(list(room.boundary.coords))
    # logger.debug("%s, %s" % (max_coverage_point, max_coverage_min_radius))
    # logger.debug(max_coverage_point)
    return max_coverage_point.coords[0] if max_coverage_point is not None else None, \
           max_coverage_min_radius, max_coverage


# return [polygon1, polygon2,polygon3]
def split_room(polygon, fraction=2):
    result_polygons = []
    if fraction < 1:
        raise Exception("error setting for fraction: %s" % fraction)
    elif fraction == 1:
        return [polygon]

    def __recursive_splitting(__polygon, __fraction):
        min_fraction = __fraction >> 1
        max_fraction = __fraction - min_fraction
        split_polygons = __min_alpha_split(__polygon, scale=(min_fraction, max_fraction))[2]
        min_polygon = (split_polygons[0] if split_polygons[0].area < split_polygons[1].area else split_polygons[1])
        max_polygon = (split_polygons[0] if split_polygons[0].area >= split_polygons[1].area else split_polygons[1])
        if min_fraction <= 1:
            result_polygons.append(min_polygon)
        else:
            __recursive_splitting(min_polygon, min_fraction)
        if max_fraction <= 1:
            result_polygons.append(max_polygon)
        else:
            __recursive_splitting(max_polygon, max_fraction)

    __recursive_splitting(polygon, fraction)
    return result_polygons


# return: split_line_point1, split_line_point2, split_polygons
def __min_alpha_split(polygon, scale=(1, 1), plot_best=False, plot_procedure=False):
    room = polygon

    def get_alpha(length, area):
        return (length ** 2) / (4 * math.pi * area)

    def get_alpha_area(area1, area2, area_scale):
        if area_scale is None:
            return 1
        min_area = min(area1, area2)
        max_area = max(area1, area2)
        _scale = max(_i for _i in area_scale) / min(_i for _i in area_scale)
        return (((max_area / min_area) - _scale) / _scale) ** 2

    min_x, min_y, max_x, max_y = room.bounds
    min_x = math.floor(min_x)
    min_y = math.floor(min_y)
    max_x = math.ceil(max_x)
    max_y = math.ceil(max_y)
    if room.area < 8:
        box_points = []
        # add outer box points
        for x in range(int(min_x, ), int(max_x + 1)):
            box_points.append((x, min_y))
            box_points.append((x, max_y))
        for y in range(int(min_y), int(max_y + 1)):
            box_points.append((min_x, y))
            box_points.append((max_x, y))
        # iterate outer box points
        best_split_point1 = None
        best_split_point2 = None
        min_alpha = float('inf')
        for i in range(len(box_points)):
            for j in range(i + 1, len(box_points)):
                split_line = LineString([box_points[i], box_points[j]])
                split_polygons = split(room, split_line)
                if len(split_polygons) != 2:
                    continue
                else:
                    alpha = sum([get_alpha(polygon.length, polygon.area) for polygon in split_polygons]) + \
                            get_alpha_area(split_polygons[0].area, split_polygons[1].area, area_scale=scale)
                    if alpha < min_alpha:
                        min_alpha = alpha
                        best_split_point1 = box_points[i]
                        best_split_point2 = box_points[j]
        split_line = LineString([best_split_point1, best_split_point2])
        split_polygons = split(room, split_line)
        if plot_procedure or plot_best:
            plot(polygons=split_polygons, lines=[split_line],
                 title="scale %s" % (split_polygons[0].area / split_polygons[1].area))
        return best_split_point1, best_split_point2, split_polygons
    else:
        halved_room = affinity.scale(room, xfact=0.5, yfact=0.5, origin=(0.0, 0.0))

        point1, point2 = __min_alpha_split(halved_room, plot_procedure=plot_procedure, scale=scale)[:2]

        x1, y1 = point1[0] << 1, point1[1] << 1
        x2, y2 = point2[0] << 1, point2[1] << 1
        x1 = max(min_x, x1)
        x1 = min(max_x, x1)
        x2 = max(min_x, x2)
        x2 = min(max_x, x2)
        y1 = max(min_y, y1)
        y1 = min(max_y, y1)
        y2 = max(min_y, y2)
        y2 = min(max_y, y2)
        box_point1s = [(x1 - 1, y1), (x1, y1), (x1 + 1, y1), (x1, y1 - 1), (x1, y1 + 1)]
        box_point2s = [(x2 - 1, y2), (x2, y2), (x2 + 1, y2), (x2, y2 - 1), (x2, y2 + 1)]
        # iterate outer box points
        best_split_point1 = None
        best_split_point2 = None
        min_alpha = float('inf')
        for point1 in box_point1s:
            for point2 in box_point2s:
                split_line = LineString([point1, point2])
                split_polygons = split(room, split_line)
                if len(split_polygons) != 2:
                    continue
                else:
                    alpha = sum(
                        [get_alpha(polygon.length, polygon.area) for polygon in split_polygons]) + get_alpha_area(
                        split_polygons[0].area, split_polygons[1].area, area_scale=scale)
                    if alpha < min_alpha:
                        min_alpha = alpha
                        best_split_point1 = point1
                        best_split_point2 = point2
        split_line = LineString([best_split_point1, best_split_point2])
        split_polygons = split(room, split_line)
        if plot_procedure or plot_best:
            plot(polygons=split_polygons, lines=[split_line],
                 title="scale %s" % (split_polygons[0].area / split_polygons[1].area))
        return best_split_point1, best_split_point2, split_polygons


# circles: [circle1(x,y,radius,...),circle2(x,y,radius,...)...]
def __get_antennas_coverage(polygon, circles):
    shapely_circles = MultiPolygon([Point(circle[0], circle[1]).buffer(circle[2]) for circle in circles])
    coverage = cascaded_union(shapely_circles).intersection(polygon)
    return coverage.area


# return: circle1(x,y,radius), circle2(x,y,radius), coverage
def double_antennas_disposition_scale(polygon, max_radius,
                                      interference_level=config.INTERFERENCE_LEVEL,
                                      plot_best=False, plot_procedure=False):
    room = polygon
    box_point1s = []
    box_point2s = []
    if room.area < 4:
        min_x, min_y, max_x, max_y = room.bounds
        min_x = int(math.ceil(min_x))
        min_y = int(math.ceil(min_y))
        max_x = int(math.floor(max_x))
        max_y = int(math.floor(max_y))
        for x in range(min_x, max_x + 1):
            for y in range(min_y, max_y + 1):
                if interference_level > 1:
                    if Point(x, y).within(room):
                        box_point1s.append((x, y))
                        box_point2s.append((x, y))
                else:
                    if Point(x, y).intersects(room):
                        box_point1s.append((x, y))
                        box_point2s.append((x, y))
    else:
        halved_room = affinity.scale(room, xfact=0.5, yfact=0.5, origin=(0, 0))
        halved_circle1, halved_circle2 = double_antennas_disposition_scale(halved_room,
                                                                           max_radius=max_radius / 2.0,

                                                                           interference_level=interference_level,
                                                                           plot_procedure=plot_procedure)[:2]
        if halved_circle1 is not None and halved_circle2 is not None:
            halved_circle1 = (halved_circle1[0] << 1, halved_circle1[1] << 1)
            halved_circle2 = (halved_circle2[0] << 1, halved_circle2[1] << 1)
            for delta_x in range(-1, 2):
                for delta_y in range(-1, 2):
                    box_point1s.append((halved_circle1[0] + delta_x, halved_circle1[1] + delta_y))
                    box_point2s.append((halved_circle2[0] + delta_x, halved_circle2[1] + delta_y))

        else:
            min_x, min_y, max_x, max_y = room.bounds
            min_x = int(math.ceil(min_x))
            min_y = int(math.ceil(min_y))
            max_x = int(math.floor(max_x))
            max_y = int(math.floor(max_y))
            for x in range(10 * min_x, 10 * max_x + 1):
                for y in range(min_y, 10 * max_y + 1):
                    if interference_level > 1:
                        if Point(x, y).within(room):
                            box_point1s.append((x, y))
                            box_point2s.append((x, y))
                    else:
                        if Point(x, y).intersects(room):
                            box_point1s.append((x, y))
                            box_point2s.append((x, y))

    max_coverage = float('-inf')
    min_interference = float('inf')
    nearest_centroid_distance = float('inf')
    max_coverage_circle1 = None
    max_coverage_circle2 = None
    logger.debug("current: %s" % list(room.boundary.coords))
    for point1 in box_point1s:
        distance1 = room.boundary.distance(Point(point1)) if interference_level > 1 else room.hausdorff_distance(
            Point(point1))
        radius1 = max_radius if distance1 > max_radius else distance1
        circle1 = Point(point1).buffer(radius1)
        for point2 in box_point2s:
            distance2 = room.boundary.distance(Point(point2)) if interference_level > 1 else room.hausdorff_distance(
                Point(point2))
            radius2 = max_radius if distance2 > max_radius else distance2
            circle2 = Point(point2).buffer(radius2)
            union = cascaded_union((circle1, circle2))
            coverage = union.intersection(room).area
            interference = union.difference(room).area
            centroid_distance = Point(point1).distance(room.centroid) ** 2 + Point(point2).distance(room.centroid) ** 2
            # 1. choose the two points who covers more area
            if coverage > max_coverage:
                max_coverage = coverage
                min_interference = interference
                nearest_centroid_distance = centroid_distance
                max_coverage_circle1 = point1 + (radius1,)
                max_coverage_circle2 = point2 + (radius2,)
            elif coverage == max_coverage:
                # 2. choose the two points who have smaller interference
                if interference < min_interference:
                    min_interference = interference
                    nearest_centroid_distance = centroid_distance
                    max_coverage_circle1 = point1 + (radius1,)
                    max_coverage_circle2 = point2 + (radius2,)
                elif interference == min_interference:
                    # 3. choose the point who is nearer to the centroid
                    if centroid_distance < nearest_centroid_distance:
                        nearest_centroid_distance = centroid_distance
                        max_coverage_circle1 = point1 + (radius1,)
                        max_coverage_circle2 = point2 + (radius2,)
    logger.debug("best %s %s" % (max_coverage_circle1, max_coverage_circle2))
    if plot_procedure or plot_best:
        plot(polygons=[room, Point(max_coverage_circle1[:2]).buffer(max_coverage_circle1[2]),
                       Point(max_coverage_circle2[:2]).buffer(max_coverage_circle2[2])],
             points=[max_coverage_circle1[:2], max_coverage_circle2[:2]])
    return max_coverage_circle1, max_coverage_circle2, max_coverage


def set_splitting(map_):
    i = 0
    for room in map_.rooms:
        i += 1
        logger.info("set splitting for room %s/%s" % (i, len(map_.rooms)))
        circles = disposition(polygon=Polygon(room.vertices), max_antenna_num=config.MAX_ANTENNAS_NUM_IN_ROOM,
                              coverage_gain_threshold=0.3)
        if len(circles) > 1:
            old_antenna = room.antennas[0]
            map_.antennas.remove(old_antenna)
            room.level = "splitting"
            for circle in circles:
                if config.INTERFERENCE_LEVEL in [0, 2]:
                    new_antenna = Antenna(coord=circle[:2], belong=room, map_=map_, max_inner_radius=circle[2])
                else:
                    new_antenna = Antenna(coord=circle[:2], belong=room, map_=map_, max_outer_radius=circle[2])
                map_.antennas.append(new_antenna)


if __name__ == "__main__":
    pass
    vertices = [[0, 0], [35, 0], [35, 35], [70, 35], [70, 90], [0, 90]]
    # vertices = [[0, 0], [90, 0], [45, 45]]
    #
    room = Polygon(vertices)
    double_antennas_disposition_scale(polygon=room,plot_procedure=True,max_radius=config.MAX_RADIUS)
    while True:
        room = affinity.rotate(room, angle=90)
        disposition(polygon=room, max_radius=30, coverage_threshold=0.9, max_antenna_num=100,
                    coverage_gain_threshold=0.2,
                    plot_procedure=True, plot_best=True)

        # # single_antenna_disposition_scale(room, max_radius=10000, plot_procedure=True)
        # # double_antennas_disposition_scale(polygon=room,plot_procedure=True, max_radius=1000)
        # polygons = split_room(polygon=room, fraction=3)
        # circles = []
        # for polygon in polygons:
        #     center, radius = single_antenna_disposition_scale(polygon=polygon, container=room)[:2]
        #     print("center", center, polygon)
        #     circles.append((center[0], center[1], radius))
        # plot(polygons=polygons, circles=circles)
