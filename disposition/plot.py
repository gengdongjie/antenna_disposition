import numpy as np
from matplotlib.patches import Circle as mat_Circle
from matplotlib.patches import Polygon as mat_Polygon
from matplotlib.collections import PatchCollection
import matplotlib.pyplot as plt
from shapely.geometry import Point, Polygon, LineString


def plot(points=None, lines=None, polygons=None, circles=None, title=None, show_text=True):
    points = [] if points is None else points
    lines = [] if lines is None else lines
    polygons = [] if polygons is None else polygons
    circles = [] if circles is None else circles

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    min_x = float("inf")
    max_x = float("-inf")
    min_y = float("inf")
    max_y = float("-inf")
    patches = []

    for circle in circles:
        min_x = min(min_x, circle[0] - circle[2])
        max_x = max(max_x, circle[0] + circle[2])
        min_y = min(min_y, circle[1] - circle[2])
        max_y = max(max_y, circle[1] + circle[2])
        points.append([circle[0], circle[1]])
        patches.append(mat_Circle((circle[0], circle[1]), circle[2]))
    for polygon in polygons:
        if type(polygon) is Polygon:
            polygon = list(polygon.boundary.coords)
        for vertex in polygon:
            min_x = min(min_x, vertex[0])
            max_x = max(max_x, vertex[0])
            min_y = min(min_y, vertex[1])
            max_y = max(max_y, vertex[1])
        polygon = mat_Polygon(polygon, True)
        patches.append(polygon)
    colors = [i for i in range(0, len(patches) + 1)]
    # colors = 100 * np.random.rand(len(patches))
    p = PatchCollection(patches, alpha=0.4)
    p.set_array(np.array(colors))
    ax.add_collection(p)

    for line in lines:
        if type(line) is LineString:
            line = list(line.coords)
        line_x = [point[0] for point in line]
        line_y = [point[1] for point in line]
        ax.plot(line_x, line_y, 'o-', linewidth=1, markersize=3)

    for point in points:
        if type(point) is Point:
            point = (point.x, point.y)
        min_x = min(min_x, point[0])
        max_x = max(max_x, point[0])
        min_y = min(min_y, point[1])
        max_y = max(max_y, point[1])
        ax.plot(point[0], point[1], 'o', markersize=3)
        if show_text:
            ax.text(point[0], point[1], "%.1f,%.1f" % (point[0], point[1]))

    ax.autoscale()
    min_lim = min(ax.get_xlim()[0], ax.get_ylim()[0])
    max_lim = max(ax.get_xlim()[1], ax.get_ylim()[1])
    ax.set_xlim(min_lim, max_lim)
    ax.set_ylim(min_lim, max_lim)
    ax.set_aspect('equal')
    if title is not None:
        ax.set_title(title)
    plt.show()

# point1 = Point(5, 6)
# point2 = Point(3, 4)
# circle = Point(1, 2).buffer(4)
# polygon = Polygon([(1, 1), (20, 1), (10, 10)])
# plot(lines=[LineString([[1, 1], [3, 4], [7, 8]])], points=[point1, point2], polygons=[circle, polygon])
