import math

from shapely import affinity
from shapely.geometry import Point, Polygon, LineString
from shapely.ops import cascaded_union

import config
from disposition.plot import plot
from logger import logger
from structure.antenna import Antenna
from structure.node import Node


def set_node_prevs_and_nexts(start_node):
    checked_nodes = []

    def __set_node_prevs_and_nexts(__start_node):
        if __start_node in checked_nodes:
            return
        checked_nodes.append(__start_node)
        for connected_node in __start_node.connected_nodes:
            if __start_node in connected_node.next_nodes:
                continue
            __start_node.add_next_node(connected_node)
            connected_node.add_prev_node(__start_node)
        for connected_node in __start_node.connected_nodes:
            __set_node_prevs_and_nexts(connected_node)

    __set_node_prevs_and_nexts(start_node)


def set_auxiliary(map_, start_node_id=config.START_NODE_ID):
    start_node = map_.nodes_dict[start_node_id]
    connected_essentials_nodes = __get_connected_essentials_nodes(start_node)
    if len(connected_essentials_nodes) >= 2:
        union_essential_outer_circles = Polygon([])
        union_essential_inner_circles = Polygon([])
        for connected_essentials_node in connected_essentials_nodes:
            if connected_essentials_node.type == "door":
                antenna = connected_essentials_node.room.antennas[0]
            else:
                antenna = connected_essentials_node.antenna
            if antenna is not None:
                outer_circle = Point(antenna.coord).buffer(antenna.outer_radius)
                inner_circle = Point(antenna.coord).buffer(antenna.inner_radius)
                if outer_circle.intersects(union_essential_outer_circles):
                    start_node.level = "auxiliary"
                union_essential_outer_circles = union_essential_outer_circles.union(outer_circle)
                union_essential_inner_circles = union_essential_inner_circles.union(inner_circle)
        if start_node.level == "auxiliary":
            radius = Point(start_node.coord).distance(union_essential_inner_circles)
            # the auxiliary point is not in rooms
            if Point(start_node.coord).within(map_.union_walls):
                radius = min(radius, Point(start_node.coord).distance(map_.union_rooms))
                if radius < 20:
                    radius = Point(start_node.coord).distance(map_.union_rooms)
                map_.antennas.append(Antenna(start_node.coord, start_node, map_, max_inner_radius=radius))
            else:
                radius = min(radius, Point(start_node.coord).distance(map_.union_walls))
                map_.antennas.append(Antenna(start_node.coord, start_node, map_, max_outer_radius=radius))

    for next_node in start_node.next_nodes:
        set_auxiliary(map_, next_node.id)


def __get_connected_essentials_nodes(__node):
    if __node.level == "essential":
        return [__node]
    elif __node.level == "auxiliary":
        return []
    connected_essentials_nodes = []
    for next_node in __node.next_nodes:
        if next_node.level == "essential":
            connected_essentials_nodes.append(next_node)
    for prev_node in __node.prev_nodes:
        connected_essentials_nodes += __get_connected_essentials_nodes(prev_node)


    return list(set(connected_essentials_nodes))


def set_complementary(map_, start_node_id=config.START_NODE_ID):
    global union_antennas
    union_antennas = cascaded_union([Point(antenna.coord).buffer(antenna.inner_radius) for antenna in map_.antennas])
    start_node = map_.nodes_dict[start_node_id]

    def __set_1st_complementary(node):
        global union_antennas
        if node.antenna is None:
            max_inner_radius = Point(node.coord).distance(union_antennas)
            max_outer_radius = config.MAX_RADIUS
            point = Point(node.coord)
            # indoor
            if point.within(map_.union_walls):
                max_inner_radius = min(max_outer_radius, point.distance(map_.union_walls.boundary))
                max_outer_radius = min(max_inner_radius, point.distance(map_.union_rooms.boundary))
            # on the wall
            elif Point(node.coord).intersects(map_.union_walls):
                max_outer_radius = min(max_outer_radius, point.distance(map_.union_rooms.boundary))
            # outdoor
            else:
                max_outer_radius = min(max_outer_radius, point.distance(map_.union_walls.boundary))
            if Antenna.get_inner_and_outer_circle(max_inner_radius=max_inner_radius,
                                                  max_outer_radius=max_outer_radius)[0] > 10:
                node.level = "complementary"
                antenna = Antenna(node.coord, node, map_, max_inner_radius=max_inner_radius,
                                  max_outer_radius=max_outer_radius)
                map_.antennas.append(antenna)
                union_antennas = union_antennas.union(point.buffer(antenna.inner_radius))

        for next_node in node.next_nodes:
            __set_1st_complementary(next_node)

    __set_1st_complementary(start_node)


def set_2nd_complementary(map_, start_node_id=config.START_NODE_ID):
    global union_antenna_inner_circles, union_antenna_outer_circles, new_nodes
    union_antenna_inner_circles = cascaded_union(
        [Point(antenna.coord).buffer(antenna.inner_radius) for antenna in map_.antennas])
    union_antenna_outer_circles = cascaded_union(
        [Point(antenna.coord).buffer(antenna.outer_radius) for antenna in map_.antennas])
    start_node = map_.nodes_dict[start_node_id]
    new_nodes = []

    def __set_2nd_complementary(node):
        global union_antenna_inner_circles, union_antenna_outer_circles
        next_nodes_copy = node.next_nodes.copy()
        if node.type != "2nd_complementary":
            for next_node in next_nodes_copy:
                if next_node.type == "2nd_complementary":
                    continue
                truncated_line = LineString((node.coord, next_node.coord)).difference(union_antenna_outer_circles)
                if not truncated_line.is_empty:
                    if truncated_line.length < 10:
                        continue
                    radius1 = node.antenna.outer_radius if node.antenna is not None else float('inf')
                    radius2 = next_node.antenna.outer_radius if next_node.antenna is not None else float('inf')

                    new_node_num = math.ceil(truncated_line.length / (2 * min(radius1, radius2)))
                    if new_node_num > 0:
                        head_node = node
                        for i in range(1, new_node_num + 1):
                            coord = affinity.scale(Point(truncated_line.coords[1]),
                                                   xfact=(2 * i - 1) / (2 * new_node_num),
                                                   yfact=(2 * i - 1) / (2 * new_node_num),
                                                   origin=truncated_line.coords[0])

                            node_dict = {'id': Node.next_id, 'coord': (coord.x, coord.y), "type": "2nd_complementary",
                                         "level": "complementary"}
                            new_node = Node(node_dict)
                            map_.nodes_dict[new_node.id] = new_node
                            # remove connection between head node and next node
                            head_node.next_nodes.remove(next_node)
                            next_node.prev_nodes.remove(head_node)
                            # create connection between head_node and new node
                            head_node.next_nodes.append(new_node)
                            new_node.prev_nodes.append(head_node)
                            # create connection between new_node and next node
                            new_node.next_nodes.append(next_node)
                            next_node.prev_nodes.append(new_node)
                            new_nodes.append(new_nodes)
                            head_node = new_node

        for next_node in node.next_nodes:
            __set_2nd_complementary(next_node)

    __set_2nd_complementary(start_node)


if __name__ == "__main__":
    pass
