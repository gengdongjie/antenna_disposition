import logging
from logging import config

config.dictConfig({
    'version': 1,
    # Other configs ...
    'disable_existing_loggers': True
})
for _ in ("matplotlib", "shapely.geos", "urllib3"):
    logging.getLogger(_).setLevel(logging.CRITICAL)
logging.basicConfig(format='%(name)s %(asctime)s,%(msecs)d %(levelname)-2s [%(filename)s:%(lineno)d] %(message)s',
                    datefmt='%d-%m-%Y:%H:%M:%S',
                    level=logging.NOTSET)

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
