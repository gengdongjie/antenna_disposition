import uuid

from shapely.geometry import Polygon


class Wall:
    __definition = None
    __wall = None
    __wall_id = None
    __wall_type = None
    __vertices = None
    __text_position = None

    def __init__(self, definition, wall):
        self.__definition = definition
        self.__wall = wall
        self.__id = wall["id"]
        self.__uuid = uuid.uuid1()
        self.__wall_type = definition["wallType"]
        self.__shapely_polygon = self.__init_shapely()

    def __init_shapely(self):
        shapely_polygon = Polygon(self.__wall['vertices'])

        return shapely_polygon

    @property
    def id(self):
        return self.__id

    @property
    def uuid(self):
        return self.__uuid

    @property
    def wall_type(self):
        return self.__wall_type

    @property
    def vertices(self):
        return list(self.__shapely_polygon.boundary.coords)

    @property
    def text_coord(self):
        return self.vertices[0]
