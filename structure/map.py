from shapely.geometry import Polygon, Point

from structure.room import Room
from structure.wall import Wall
from structure.node import Node
from structure.antenna import Antenna
from disposition import path_disposition, room_disposition
import config
import json
from logger import logger
import os
from shapely.geometry import Point, LineString, Polygon
from shapely.ops import split, cascaded_union
import config
from disposition.plot import plot
from shapely.geometry import Point, LineString, Polygon
from shapely.ops import split, cascaded_union, nearest_points
from shapely import speedups, affinity
from shapely.geometry import MultiPolygon

file_head = os.path.dirname(os.path.realpath(__file__)) + '/../'


class Map:
    def __init__(self):
        self.__rooms_dict = self.__load_rooms()
        self.__union_rooms = cascaded_union([Polygon(room.vertices) for room in self.rooms])
        self.__walls = self.__load_walls()
        self.__union_walls = cascaded_union([Polygon(wall.vertices) for wall in self.walls])

        self.__nodes_dict = self.__load_nodes()
        self.__antennas = self.__get_essential_antennas()
        # set for different levels
        self.__set_auxiliary_antennas()
        # self.__set_complementary_antennas()
        # self.__set_splitting_antennas()

    @staticmethod
    def __load_rooms():

        with open(file_head + config.ROOMS) as f_obj:
            rooms_data = json.load(f_obj)
        rooms = {}
        for structure in rooms_data["structures"]:
            definition = structure["definition"]
            for room in structure["rooms"]:
                rooms[room['id']] = Room(definition, room)
        return rooms

    @staticmethod
    def __load_walls():
        with open(file_head + config.WALLS) as f_obj:
            walls_data = json.load(f_obj)
        walls = []
        for structure in walls_data["structures"]:
            definition = structure["definition"]
            for wall in structure["walls"]:
                walls.append(Wall(definition, wall))
        return walls

    def __load_nodes(self):
        with open(file_head + config.PATHS) as f_obj:
            nodes_data = json.load(f_obj)
        nodes = {}

        for node in nodes_data["nodes"]:
            if node['type'] == "door":
                new_node = Node(node, self.rooms_dict[node['room']])
            else:
                new_node = Node(node)
            if new_node.id in nodes:
                raise Exception("multiple node exist for id %s" % new_node.id)
            nodes[new_node.id] = new_node
        for node in nodes_data["nodes"]:
            if 'connected' in node:
                for connected_node in node['connected']:
                    nodes[node['id']].add_connected_node(nodes[connected_node])
                    nodes[connected_node].add_connected_node(nodes[node['id']])

        path_disposition.set_node_prevs_and_nexts(nodes[config.START_NODE_ID])

        return nodes

    def __get_essential_antennas(self):
        antennas = []
        union_essentials = Polygon([])
        # get essential antennas in rooms
        for room in self.rooms:
            circles = room_disposition.disposition(Polygon(room.vertices), max_antenna_num=1)
            for circle in circles:
                if config.INTERFERENCE_LEVEL in [0, 2]:
                    new_antenna = Antenna(circle[:2], room, self, max_inner_radius=circle[2])
                else:
                    new_antenna = Antenna(circle[:2], room, self, max_outer_radius=circle[2])
                union_essentials = union_essentials.union(Point(circle[:2]).buffer(new_antenna.inner_radius))
                antennas.append(new_antenna)
                room.antennas.append(new_antenna)
        for node in self.nodes:
            if node.type == "door":
                continue
            elif node.level == "essential":
                point = Point(node.coord)
                radius = float("inf")
                radius = min(radius, point.distance(union_essentials))
                # if the node is indoor
                if point.intersects(self.union_walls):
                    radius = min(radius, point.distance(self.union_rooms))
                # if the node is outdoor
                else:
                    radius = min(radius, point.distance(self.union_walls))
                new_antenna = Antenna(node.coord, node, self, max_outer_radius=radius)
                antennas.append(new_antenna)
        return antennas

    def __set_auxiliary_antennas(self):
        path_disposition.set_auxiliary(map_=self)

    def __set_complementary_antennas(self):
        path_disposition.set_complementary(map_=self)
        path_disposition.set_2nd_complementary(map_=self)
        path_disposition.set_complementary(map_=self)

    def __set_splitting_antennas(self):
        room_disposition.set_splitting(map_=self)

    @property
    def rooms_dict(self):
        return self.__rooms_dict

    @property
    def rooms(self):
        return self.__rooms_dict.values()

    @property
    def union_rooms(self):
        return self.__union_rooms

    @property
    def walls(self):
        return self.__walls

    @property
    def union_walls(self):
        return self.__union_walls

    @property
    def nodes_dict(self):
        return self.__nodes_dict

    @property
    def nodes(self):
        return self.__nodes_dict.values()

    @property
    def antennas(self):
        return self.__antennas

    # update_name=[(old_id,new_id),(old_id,new_id),....]
    # update_essential =[(node_id, T/F),(node_id, T/F),....]
    @staticmethod
    def _tidy_nodes(update_nodes=None, delete_nodes=None):
        if id_update_dict is None: update_nodes = {}
        if delete_nodes is None: delete_nodes = []
        with open(file_head + config.PATHS, 'r+') as f_obj:
            nodes_data = json.load(f_obj)
            nodes = []
            for node in nodes_data["nodes"]:
                nodes.append(node)
            # step 1: update old node
            for node in nodes.copy():
                if node['id'] in update_nodes:
                    node['id'] = update_nodes[node['id']]
                    if 'connected' in node:
                        for connected_id in node['connected'].copy():
                            if connected_id in update_nodes:
                                node['connected'].remove(connected_id)
                                node['connected'].append(update_nodes[connected_id])
            # step 2: delete nodes
            for node in nodes.copy():
                if node['id'] in delete_nodes:
                    nodes_data["nodes"].remove(node)
                elif 'connected' in node:
                    for connected_id in node['connected'].copy():
                        if connected_id in delete_nodes:
                            node['connected'].remove(connected_id)

            # step 3: remove disordered and duplicate connection
            for node in nodes.copy():
                if 'connected' in node:
                    for connected_id in node['connected'].copy():
                        if connected_id < node['id']:
                            node['connected'].remove(connected_id)
                            for _node in nodes.copy():
                                if _node['id'] == connected_id:
                                    if 'connected' not in _node:
                                        _node['connected'] = [node['id']]
                                    else:
                                        _node['connected'].append(node['id'])
                        elif connected_id == node['id']:
                            node['connected'].remove(connected_id)
            # step 4: sort all connected nodes
            for node in nodes_data["nodes"]:
                if 'connected' in node:
                    if len(node['connected']) == 0:
                        del node['connected']
                    else:
                        node['connected'] = sorted(list(set(node['connected'])))
            f_obj.seek(0)
            nodes_data["nodes"] = sorted(nodes_data["nodes"], key=lambda x: x["id"])
            json.dump(nodes_data, f_obj, indent=2)
            f_obj.truncate()


if __name__ == "__main__":
    map_ = Map()
    id_update_dict = {}
    delete_nodes = []
    Map._tidy_nodes(update_nodes=id_update_dict, delete_nodes=delete_nodes)

    # for id_, node in map_.nodes.items():
    #     print(node.id, node.node_type)
