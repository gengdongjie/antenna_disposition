import uuid

from shapely.geometry import Point


class Node:
    next_id = 0

    def __init__(self, node, room=None):
        self.__id = node['id']
        Node.next_id = self.id + 1
        self.__uuid = uuid.uuid1()
        self.__coord = node['coord']
        self.__type = node['type']
        self.__room = room if self.__type == "door" else None
        if "level" in node:
            self.__level = node['level']
        elif room is not None:
            self.__level = room.level
        else:
            self.__level = None
        self.__antenna = None
        self.__connected_nodes = []
        self.__prev_nodes = []
        self.__next_nodes = []

    @property
    def id(self):
        return self.__id

    @property
    def uuid(self):
        return self.__uuid

    @property
    def coord(self):
        return self.__coord

    @property
    def type(self):
        return self.__type

    @type.setter
    def type(self, _type):
        self.__type = _type

    @property
    def x(self):
        return self.__coord[0]

    @property
    def y(self):
        return self.__coord[1]

    @property
    def level(self):
        return self.__level

    @level.setter
    def level(self, level):
        self.__level = level

    @property
    def room(self):
        return self.__room

    @property
    def antenna(self):
        return self.__antenna

    @antenna.setter
    def antenna(self, new_antenna):
        self.__antenna = new_antenna

    @property
    def connected_nodes(self):
        return self.__connected_nodes

    def add_connected_node(self, node):
        if node not in self.__connected_nodes:
            self.__connected_nodes.append(node)

    @property
    def prev_nodes(self):
        return self.__prev_nodes

    def add_prev_node(self, node):
        if node not in self.__prev_nodes:
            self.prev_nodes.append(node)

    @property
    def next_nodes(self):
        return self.__next_nodes

    def add_next_node(self, node):
        if node not in self.__next_nodes:
            self.next_nodes.append(node)
