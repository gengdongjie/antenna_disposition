from shapely.geometry import Point

from logger import logger
from structure.node import Node
from structure.room import Room

import config

MAX_RADIUS = config.SENSITIVITY / config.SENSITIVITY_SCALE
MARGIN = config.SENSITIVITY_MARGIN


class Antenna:
    __id = 0

    def __init__(self, coord, belong, map_, max_inner_radius=MAX_RADIUS, max_outer_radius=MAX_RADIUS):
        Antenna.__id += 1
        self.__id += self.__id
        self.__coord = coord
        self.__map = map_
        self.__belong = belong
        self.__level = belong.level
        if type(belong) is Room:
            self.__belong_type = "room"
            belong.antennas.append(self)
        elif type(belong) is Node:
            self.__belong_type = "node"
            belong.antenna = self
        else:
            raise Exception("unknown belong for Antenna coord %s" % coord)
        self.__inner_radius, self.__outer_radius = self.get_inner_and_outer_circle(max_inner_radius, max_outer_radius)
        self.__svg_radius, self.__svg_stroke_width = self.__get_svg_radius_and_stroke()

    @staticmethod
    def get_inner_and_outer_circle(max_inner_radius, max_outer_radius):
        # calculate radius set 1 according to max_inner_radius
        inner_radius1 = min(max_inner_radius, MAX_RADIUS)
        outer_radius1 = min(inner_radius1 * (1 + MARGIN), MAX_RADIUS)
        # calculate radius set 2 according to max_outer_radius
        outer_radius2 = min(max_outer_radius, MAX_RADIUS)
        inner_radius2 = outer_radius2 / (1 + MARGIN)
        return min(inner_radius1, inner_radius2), min(outer_radius1, outer_radius2)

    def __get_svg_radius_and_stroke(self):
        svg_stroke_width = self.__outer_radius - self.__inner_radius
        svg_radius = self.__inner_radius + svg_stroke_width / 2

        return svg_radius, svg_stroke_width

    @property
    def id(self):
        return self.__id

    @property
    def coord(self):
        return self.__coord

    @property
    def belong_type(self):
        return self.__belong_type

    @property
    def belong(self):
        return self.__belong

    @property
    def level(self):
        return self.__level

    @property
    def outer_radius(self):
        return self.__outer_radius

    @property
    def inner_radius(self):
        return self.__inner_radius

    @property
    def svg_radius(self):
        return self.__svg_radius

    @property
    def svg_stroke_width(self):
        return self.__svg_stroke_width


if __name__ == "__main__":
    radius = 90
    if radius * (1 + MARGIN) > MAX_RADIUS:
        svg_stroke = (MAX_RADIUS - radius) / 2
    else:
        svg_stroke = radius * MARGIN / 2
    svg_radius = radius + svg_stroke
