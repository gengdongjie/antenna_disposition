from shapely.geometry import Polygon
from shapely import affinity
import uuid


class Room:

    def __init__(self, definition, room):
        self.__definition = definition
        self.__room = room
        self.__id = room["id"]
        self.__type = "room"
        self.__level = definition['level'] if "level" in definition else None
        self.__shapely_polygon = self.__init_shapely()
        self.__antennas = []

    def __init_shapely(self):
        if self.__definition['type'] == "customized" or self.__definition['type'] == "indoor" or \
                self.__definition['type'] == "outdoor":
            shapely_polygon = Polygon(self.__room['vertices'])
            return shapely_polygon
        shapely_polygon = Polygon(self.__definition['vertices'])
        transform = self.__room['transform']
        shapely_polygon = affinity.rotate(shapely_polygon, transform[0], origin=(0, 0))
        if transform[1] == "":
            pass
        elif transform[1] == "h":
            shapely_polygon = affinity.scale(shapely_polygon, xfact=-1, origin=(0, 0))
        elif transform[1] == "v":
            shapely_polygon = affinity.scale(shapely_polygon, yfact=-1, origin=(0, 0))
        else:
            raise ValueError("invalid transform setting for room %s" % self.id)
        shapely_polygon = affinity.translate(shapely_polygon, xoff=self.__room['root'][0],
                                             yoff=self.__room['root'][1])
        return shapely_polygon

    @property
    def id(self):
        return self.__id

    @property
    def antennas(self):
        return self.__antennas

    @property
    def type(self):
        return self.__type

    @property
    def level(self):
        return self.__level

    @level.setter
    def level(self, val):
        self.__level = val
    @property
    def vertices(self):
        return list(self.__shapely_polygon.boundary.coords)
    @property
    def centroid(self):
        return self.__shapely_polygon.centroid.coords[0]
    @property
    def text_coord(self):
        return self.centroid
