import config
from logger import logger
from structure.map import Map
import config
from disposition.plot import plot
from shapely.geometry import Point, LineString, Polygon
from shapely.ops import split, cascaded_union
from shapely import speedups, affinity
from shapely.geometry import MultiPolygon


class SvgHelper:
    def __init__(self, width=config.SVG_WIDTH, height=config.SVG_HEIGHT, style=config.SVG_STYLE,
                 view_box=config.SVG_VIEW_BOX):
        self.__svg = {'width': width, 'height': height, 'style': style, 'viewBox': view_box}
        self.__polygons = []
        self.__circles = []
        self.__texts = []
        self.__lines = []
        map_ = Map()
        self.__add_rooms(map_.rooms)
        self.__add_walls(map_.walls)
        self.__add_nodes(map_.nodes)
        self.__add_antennas(map_.antennas)

    def __add_rooms(self, rooms):
        for room in rooms:
            if room.type != "hidden":
                self.add_polygon(room.vertices, config.SHOW_ROOM_VERTICES)
                self.add_text(room.text_coord, "r" + str(room.id))

    def __add_walls(self, walls):
        for wall in walls:
            self.add_polygon(wall.vertices, config.SHOW_WALL_VERTICES)
            if not config.SHOW_WALL_VERTICES:
                self.add_text(wall.text_coord, "w" + str(wall.id))

    def __add_nodes(self, nodes, show_coord=True):
        for node in nodes:
            for connected_node in node.connected_nodes:
                self.add_lines(node.coord, connected_node.coord, stroke="#133aac", stroke_width=2)
            if show_coord:
                self.add_text(point=node.coord, content="n%s" % node.id)
            if node.level == "essential":
                self.add_circle(point=(node.x, node.y), r=5, stroke_width=0.5, fill="#ff3900")
            elif node.level == "auxiliary":
                self.add_circle(point=(node.x, node.y), r=5, stroke_width=0.5, fill="#48dd00")
            elif node.level == "complementary":
                self.add_circle(point=(node.x, node.y), r=5, stroke_width=0.5, fill="#427df4")
            elif node.type == "splitting":
                self.add_circle(point=(node.x, node.y), r=5, stroke_width=0.5, fill="#c1acc6")
            else:
                self.add_circle(point=(node.x, node.y), r=5, stroke_width=0.5, fill="#bfa730")

    def __add_antennas(self, antennas):
        for antenna in antennas:
            if antenna.level == "essential":
                fill = "#9bff66"
            elif antenna.level == "auxiliary":
                fill = "#ff4100"
            elif antenna.level == "complementary":
                fill = "#427df4"
            elif antenna.level == "splitting":
                fill = "#c1acc6"
            else:
                fill = "#9bff66"

            self.add_circle(antenna.coord, r=antenna.svg_radius,
                            stroke_width=antenna.svg_stroke_width,
                            stroke="#ffe566", fill=fill, opacity=0.3)
            self.add_circle(antenna.coord, r=2, fill="#52a529")

    def add_polygon(self, vertices, show_vertices=False, fill="none", stroke="black", stroke_width=2):
        style = ""
        for k, v in locals().items():
            if k not in ['self', 'style', 'vertices', 'show_vertices']:
                style += "%s:%s;" % (k.replace('_', '-'), v)

        points = ""
        for vertex in vertices:
            points = points + ("%d,%d " % (vertex[0], vertex[1]))
        self.__polygons.append({'points': points, 'style': style})
        if show_vertices:
            i = 1
            for vertex in vertices:
                self.add_text(vertex, content="%s:%d,%d" % (i, vertex[0], vertex[1]))
                i += 1

    def add_lines(self, point1, point2, stroke="black", stroke_width=0):
        style = ""
        for k, v in locals().items():
            if k not in ['self', 'style', 'point1', 'point2']:
                style += "%s:%s;" % (k.replace('_', '-'), v)
        self.lines.append({'point1': point1, 'point2': point2, 'style': style})

    def add_text(self, point, content, fill="red"):
        style = ""
        for k, v in locals().items():
            if k not in ['self', 'style', 'point', 'content']:
                style += "%s:%s;" % (k.replace('_', '-'), v)

        self.__texts.append({'point': (point[0], point[1]), 'content': content, 'style': style})

    def add_circle(self, point, r=2, stroke="black", stroke_width=0.5, fill="red", opacity=1.0):
        style = ""
        for k, v in locals().items():
            if k not in ['self', 'style', 'point', 'r']:
                style += "%s:%s;" % (k.replace('_', '-'), v)
        self.__circles.append(
            {'point': point, 'r': r, 'style': style})

    @property
    def svg(self):
        return self.__svg

    @property
    def polygons(self):
        return self.__polygons

    @property
    def circles(self):
        return self.__circles

    @property
    def lines(self):
        return self.__lines

    @property
    def texts(self):
        return self.__texts
