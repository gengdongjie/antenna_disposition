import math
import random
import time

from logger import logger
from structure.map import Map
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap, ListedColormap, BoundaryNorm
from matplotlib.pyplot import figure
import config
from disposition.plot import plot
from shapely.geometry import Point, LineString, Polygon
from shapely.ops import split, cascaded_union, nearest_points
from shapely import speedups, affinity
from shapely.geometry import MultiPolygon
import os
from matplotlib.patches import Polygon as mat_Polygon
from matplotlib.collections import PatchCollection


def __update_heat_map(heat_map, point, radius, margin):
    infinity = config.MAX_RADIUS - radius - margin
    if margin == 0:
        radius = 0.8 * config.MAX_RADIUS
        margin = config.MAX_RADIUS - config.MAX_RADIUS
        infinity = 0.4 * config.MAX_RADIUS
    else:
        if radius + margin >= config.MAX_RADIUS - 1:
            infinity = margin * 0.2
            margin *= 0.8

    radius_delta = radius / 16
    margin_delta = margin / 6
    infinity_delta = infinity / 13
    for x in range(0, int(config.MAX_RADIUS + 1)):
        for y in range(0, int(config.MAX_RADIUS + 1)):
            distance = (x ** 2 + y ** 2) ** 0.5
            if distance > config.MAX_RADIUS:
                continue
            elif distance <= radius:
                val = distance / radius_delta
            elif distance <= radius + margin:
                val = (distance - radius) / margin_delta + 16
            else:
                val = (distance - radius - margin) / infinity_delta + 16 + 6
            # check x
            cx = int(point[0])
            cy = int(point[1])
            if cx + x < heat_map.shape[0]:
                if cy + y < heat_map.shape[1]:
                    heat_map[cx + x][cy + y] = min(heat_map[cx + x][cy + y], val)
                if cy - y >= 0:
                    heat_map[cx + x][cy - y] = min(heat_map[cx + x][cy - y], val)
            if cx - x >= 0:
                if cy + y < heat_map.shape[1]:
                    heat_map[cx - x][cy + y] = min(heat_map[cx - x][cy + y], val)
                if cy - y >= 0:
                    heat_map[cx - x][cy - y] = min(heat_map[cx - x][cy - y], val)


def __get_polygons_collection(polygons):
    patches = []
    for polygon in polygons:
        patches.append(mat_Polygon(polygon.vertices, True))

    p = PatchCollection(patches, alpha=0.2)
    return p


def plot_heat_map(antennas, polygons):
    resolution = config.RESOLUTION
    heat_map = np.array([48]).repeat(resolution ** 2).reshape(resolution, resolution)
    for antenna in antennas:
        logger.debug("plot heatmap for antenna %s/%s..." % (antennas.index(antenna)+1, len(antennas)))
        __update_heat_map(heat_map, antenna.coord, antenna.inner_radius, antenna.outer_radius - antenna.inner_radius)

    color_dict = {13: '#26ac95', 14: '#28b08a', 15: '#2ab47e', 16: '#2db873', 17: '#2fbc67', 18: '#32bf5d',
                  19: '#35c352', 20: '#38c747', 21: '#3ccb3e', 22: '#48ce3d', 23: '#56d13c', 24: '#65d33b',
                  25: '#76d63a', 26: '#88d93a', 27: '#9bdb39', 28: '#afde39', 29: '#c4e039', 30: '#dbe339',
                  31: '#e7db38', 32: '#e9c834', 33: '#ecb330', 34: '#ee9d2c', 35: '#f08728', 36: '#f36e25',
                  37: '#f55521', 38: '#f73b1f', 39: '#fa221d', 40: '#fc0d1b', 41: '#f3111d', 42: '#e9151f',
                  43: '#de1921', 44: '#d51c23', 45: '#cb2026', 46: '#c12428', 47: '#b7262a', 48: '#ad292c'}

    colors = list(color_dict.values())
    cmap = ListedColormap(colors)
    bounds = [i + 1 for i in range(len(colors))]
    norm = BoundaryNorm(bounds, cmap.N)
    fig = figure(num=None, figsize=(15, 15), dpi=80)
    ax = fig.add_subplot(1, 1, 1)

    # ax.add_collection(__get_polygons_collection(polygons))
    plt.imshow(heat_map.T, cmap=cmap, norm=norm, origin='lower')
    plt.colorbar()
    for polygon in polygons:
        xs = [i[0] for i in polygon.vertices]
        xs.append(polygon.vertices[0][0])
        ys = [i[1] for i in polygon.vertices]
        ys.append(polygon.vertices[0][1])
        ax.plot(xs, ys, c='k', ls='-', marker='', linewidth=1, markersize=3)
    # plt.setp(plt.gca(), 'ylim', reversed(plt.getp(plt.gca(), 'ylim')))
    plt.gca().invert_yaxis()
    plt.savefig("result.svg", format="svg")
    plt.show()


if __name__ == "__main__":
    map_ = Map()
    plot_heat_map(map_.antennas[:], list(map_.rooms) + map_.walls)
