DEBUG = True  # 启动Flask的Debug模式

# initialize antennas
INIT_ANTENNA = True
# antennas setting
SENSITIVITY = 1800  # unit: centimeters
SENSITIVITY_SCALE = 6.5  # centimeter per pixel
SENSITIVITY_MARGIN = 0.5  # 0.5 * radius
MAX_RADIUS = SENSITIVITY / SENSITIVITY_SCALE

# interference level setting
# level 0: inner circle is circumcircle of room
# level 1: outer circle is circumcircle of room
# level 2: inner circle is incircle of room
# level 3: outer circle is incircle of room
INTERFERENCE_LEVEL = 2
MAX_ANTENNAS_NUM_IN_ROOM = 2
START_NODE_ID = 106
# structure setting
ROOMS = "structure_data/rooms.json"
AREAS = "structure_data/areas.json"
WALLS = "structure_data/walls.json"
PATHS = "structure_data/nodes.json"
SHOW_WALL_VERTICES = False
SHOW_ROOM_VERTICES = False

# default resolution
RESOLUTION = 1600

# Default vg setting
SVG_WIDTH = 1600
SVG_HEIGHT = 1600
SVG_STYLE = "border:0px solid #000000;display: block;"
# SVG_STYLE = "border:1px solid #000000;display: block;margin: auto;"

# default viewBox setting
SVG_VIEW_BOX = '0,0,1600,1600'

# default polygon setting
SVG_POLYGON_STYLE = ";"
