from app import app
from flask import render_template
from markupsafe import Markup
from svg_helper import SvgHelper
import config


@app.route('/')
@app.route('/index')
def hello_world():
    svg_helper = SvgHelper()
    return render_template("index.html", svg=svg_helper.svg, ploygons=svg_helper.polygons, circles=svg_helper.circles,
                           texts=svg_helper.texts, lines=svg_helper.lines)


if __name__ == '__main__':
    app.run(debug=True)
